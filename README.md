# sapper-tailwindcss-template
Sapper + Svelte + Tailwindcss

### Running the project

```bash
git clone https://gitlab.com/YorgYetson/sapper-tailwind.git my-app
cd my-app
npm install
npm run dev
```

Open up [localhost:3000](http://localhost:3000) and start clicking around.

[svelte.dev](https://svelte.dev)  
[sapper.svelte.dev](https://sapper.svelte.dev)  
[https://tailwindcss.com/docs/](https://tailwindcss.com/docs/)  